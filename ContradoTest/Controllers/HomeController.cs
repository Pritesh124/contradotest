﻿using ContradoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContradoTest.Controllers
{
    public class HomeController : Controller
    {
        private ECommerceDemoEntities db = new ECommerceDemoEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult getProduct()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var model = (db.Products.ToList()
                        .Select(x => new
                        {
                            ProdCatId = x.ProdCatId,
                            CategoryName = x.ProductCategory.CategoryName,
                            ProductId = x.ProductId,
                            ProdName = x.ProdName,
                            ProdDescription = x.ProdDescription
                        })).ToList();

            return Json(new
            {
                draw = draw,
                recordsFiltered = model.Count,
                recordsTotal = model.Count,
                data = model
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult saveProduct(ProductViewModel productdata)
        {
            if (productdata.ProductId > 0)
            {
                //Update data
                var prd = db.Products.SingleOrDefault(a => a.ProductId == productdata.ProductId);
                if (prd != null)
                {
                    prd.ProdName = productdata.ProdName;
                    prd.ProdDescription = productdata.ProdDescription;
                    db.SaveChanges();
                }

                #region delete attribute for selected product
                var prdattr = db.ProductAttributes.Where(a => a.ProductId == productdata.ProductId).ToList();
                if (prdattr != null && prdattr.Count > 0)
                {
                    db.ProductAttributes.RemoveRange(prdattr);
                    db.SaveChanges(); 
                }
                #endregion

                #region Add attribite
                if (productdata.productDetails.Any())
                {
                    List<ProductAttribute> productAttributes = new List<ProductAttribute>();
                    foreach (var item in productdata.productDetails)
                    {
                        var productDetails = new ProductAttribute()
                        {
                            ProductId = productdata.ProductId,
                            AttributeId = item.AttributeId,
                            AttributeValue = item.AttributeValue,
                        };
                        productAttributes.Add(productDetails);
                    }
                    db.ProductAttributes.AddRange(productAttributes);
                    db.SaveChanges();
                }
                #endregion
                return Json(new { error = false, message = "Product update successfully" });
            }
            else
            {
                //Insert data
                var product = new Product()
                {
                    ProdCatId = productdata.ProdCatId,
                    ProdName = productdata.ProdName,
                    ProdDescription = productdata.ProdDescription
                };
                db.Products.Add(product);
                db.SaveChanges();
                long pid = product.ProductId;
                 
                if (productdata.productDetails.Any())
                {
                    foreach (var item in productdata.productDetails)
                    {
                        var productDetails = new ProductAttribute()
                        {
                            ProductId = pid,
                            AttributeId = item.AttributeId,
                            AttributeValue = item.AttributeValue,
                        };
                        db.ProductAttributes.Add(productDetails);
                    }
                    db.SaveChanges();
                }
                return Json(new { error = false, message = "Product saved successfully" });
            }
              
            return Json(new { error = true, message = "An unknown error has occured" });
        }

        public ActionResult getSingleProduct(long ProductId)
        {
            var model = (from prd in db.Products
                         where prd.ProductId == ProductId
                         select new ProductViewModel()
                         {
                             ProdCatId = prd.ProdCatId,
                             ProductId = prd.ProductId,
                             ProdName = prd.ProdName,
                             ProdDescription = prd.ProdDescription
                         }).SingleOrDefault();

            if (model != null)
            {
                model.productDetails = (from prdattr in db.ProductAttributes
                                        where prdattr.ProductId == model.ProductId
                                        select new ProductDetailsViewModel()
                                        {
                                            ProductId = prdattr.ProductId,
                                            AttributeId = prdattr.AttributeId,
                                            AttributeName = prdattr.ProductAttributeLookup.AttributeName,
                                            AttributeValue = prdattr.AttributeValue
                                        }).ToList();
            }

            return Json(new { result = model }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult deleteProductItem(long ProductId, Int32 AttributeId)
        {
            var s = db.ProductAttributes.ToList();
            var prd = db.ProductAttributes.Where(x => x.ProductId == ProductId && x.AttributeId == AttributeId).ToList();
            if (prd != null)
            {
                db.ProductAttributes.Remove(prd.FirstOrDefault());
                db.SaveChanges();
                return Json(new { error = false });
            }
            return Json(new { error = true });
        }
        public ActionResult getSingleProductDetail(long id, long id2)
        {
            var productDetail = (from od in db.ProductAttributes
                               where od.ProductId == id && od.AttributeId == id2
                               select new ProductDetailsViewModel()
                               {
                                   ProductId = od.ProductId,
                                   ProdCatId = od.Product.ProdCatId,
                                   AttributeId = od.AttributeId,
                                   AttributeName = od.ProductAttributeLookup.AttributeName,
                                   AttributeValue = od.AttributeValue
                               }).SingleOrDefault();

            return Json(new { result = productDetail }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getproductattr(long prodcatid)
        { 
            return Json(db.ProductAttributeLookups.Where(x => x.ProdCatId == prodcatid).Select(x => new
            {
                AttributeId = x.AttributeId,
                AttributeName = x.AttributeName
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult getproductcategory()
        { 
            return Json(db.ProductCategories.Select(x => new
            {
                ProdCatId = x.ProdCatId,
                CategoryName = x.CategoryName
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
         
    }
}