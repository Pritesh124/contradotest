﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContradoTest.Models
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {
            this.productDetails = new List<ProductDetailsViewModel>();
        }
        public long ProductId { get; set; }
        public int ProdCatId { get; set; }
        public string ProdName { get; set; }
        public string ProdDescription { get; set; }
        public virtual ICollection<ProductDetailsViewModel> productDetails { get; set; }
    }

    public class ProductDetailsViewModel
    {
        public long ProductId { get; set; }
        public long ProdCatId { get; set; }
        public int AttributeId { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }
    }
}